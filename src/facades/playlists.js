import { getPlaylists } from 'src/services/api-requests'
import { usePlaylistsStore } from 'src/stores/playlists-store'
import { requestNextTrack } from 'src/facades/tracks'
import { receiveElectronEvent } from 'src/helpers/electron-event-helper'

let playlistsStore = usePlaylistsStore()

const requestPlaylists = () => {
  return getPlaylists().then((playlists) => {
    playlistsStore.list = playlists

    if (playlists.length > 0) {
      playlistsStore.currentId = playlists[0].id
    }
  })
}

const setCurrentPlaylistId = (playlistId) => {
  playlistsStore.currentId = playlistId
  requestNextTrack()
}

export { requestPlaylists, setCurrentPlaylistId }
