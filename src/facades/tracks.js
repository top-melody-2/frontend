import { useTracksStore } from 'src/stores/track-store'
import { usePlaylistsStore } from 'src/stores/playlists-store'
import { getNextTrack, getPrevTrack, likeTrack, dislikeTrack } from 'src/services/api-requests'
import { setAudioIsPlaying, setAudioSource, setCurrentTime, getIsPlaying } from 'src/facades/audio'
import { sendElectronEvent } from 'src/helpers/electron-event-helper'
import config from 'src/config'
import events from 'common/events'

let trackStore = useTracksStore()
const playlistsStore = usePlaylistsStore()

const requestNextTrack = async () => {
  trackStore.isNextTrackLoading = true
  const promise = await getNextTrack(playlistsStore.currentId).then((track) => changeCurrentTrack(track))
  
  trackStore.isNextTrackLoading = false
  return promise
}

const requestPrevTrack = async () => {
  trackStore.isPrevTrackLoading = true
  const promise = await getPrevTrack(playlistsStore.currentId).then((track) => changeCurrentTrack(track))

  trackStore.isPrevTrackLoading = false
  return promise
}

const changeCurrentTrack = (track) => {
  if (!track) {
    return
  }

  trackStore.current = track

  setCurrentTime(0)
  setAudioSource(config.apiUrl + `/tracks/${track.id}/stream`)
  setAudioIsPlaying(getIsPlaying())
}

const likeCurrentTrack = async () => {
  trackStore.isLikeTrackLoading = true
  const promise = await likeTrack(trackStore.current.id).then((message) => sendElectronEvent(window, events.showNotify, message))

  trackStore.isLikeTrackLoading = false
  return promise
}

const dislikeCurrentTrack = async () => {
  trackStore.isDislikeTrackLoading = true
  const promise = await dislikeTrack(trackStore.current.id).then((message) => {
    sendElectronEvent(window, events.showNotify, message)
    requestNextTrack()
  })
  
  trackStore.isDislikeTrackLoading = false
  return promise
}

export { requestNextTrack, requestPrevTrack, likeCurrentTrack, dislikeCurrentTrack }
