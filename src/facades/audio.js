import { setToLocalStorage } from 'src/helpers/local-storage-helper'
import { useAudioStore } from 'src/stores/audio-store'

let audioStore = useAudioStore()

const getAudio = () => {
  let audio = document.querySelector('audio')
  if (!audio) {
    audio = new Audio('')
    audio.preload = 'metadata'
    audio.crossOrigin = 'anonymous'

    // Добавляем элемент на страницу, чтобы при запуске метода play()
    // api не ругалось на отсутствие элемента на странице
    document.querySelector('body').append(audio)
  }

  return audio
}

/**
 * @param {number} level Уровень громкости от 0 до 100
 */
const setVolume = (level) => {
  setToLocalStorage('volume', level)
  setStoreVolume(level)
  setAudioVolume(level)
}

/**
 * @param {number} level Уровень громкости от 0 до 100
 */
const setAudioVolume = (level) => {
  getAudio().volume = convertVolumeLevelWithLogarithm(level)
}

/**
 * @param {number} level Уровень громкости от 0 до 100
 */
const setStoreVolume = (level) => {
  audioStore.volume = level
}

/**
 * @returns {number} level Уровень громкости от 0 до 100
 */
const getVolumeLevel = () => {
  return findVolumeLevelByLogarithmLevel(getAudio().volume)
}

/**
 * @param {number} level Уровень громкости 0..100
 * @return {number} Уровень громкости 0..1
 */
const convertVolumeLevelWithLogarithm = (level) => {
  // -log115(-x+1)
  // Логарифмическая формула расчета громкости, так слышит наше ухо
  let x = level / 100 // x := 0...1
  return +level === 100 ? 1 : -(Math.log(-x + 1) / Math.log(115))
}

/**
 * @param {number} logarithmLevel Уровень громкости 0..1
 * @return {number} Уровень громкости 0..100
 */
const findVolumeLevelByLogarithmLevel = (logarithmLevel) => {
  let closestValue = 0
  let minRange = 1
  for (let i = 0; i <= 100; i++) {
    const logarithmLevelForVolumeLevel = convertVolumeLevelWithLogarithm(i)
    const range = Math.abs(logarithmLevelForVolumeLevel - logarithmLevel)

    if (range < minRange) {
      closestValue = i
      minRange = range
    }
  }

  return closestValue
}

/**
 * @param {string} src URL для загрузки песни
 */
const setAudioSource = (src) => {
  let audio = getAudio()
  audio.src = src
  audio.load()
}

const changeIsPlaying = () => {
  setIsPlaying(!audioStore.isPlaying)
}

const setIsPlaying = (isPlaying) => {
  setStoreIsPlaying(isPlaying)
  setAudioIsPlaying(isPlaying)
}

const setStoreIsPlaying = (isPlaying) => {
  audioStore.isPlaying = isPlaying
}

const setAudioIsPlaying = (isPlaying) => {
  if (isPlaying) {
    return getAudio().play().catch(() => {})
  } else {
    return new Promise((resolve, reject) => {
      getAudio().pause()
      resolve()
    })
  }
}

const getIsPlaying = () => {
  return audioStore.isPlaying
}

const setCurrentTime = (seconds) => {
  setStoreCurrentTime(seconds)
  setAudioCurrentTime(seconds)
}

const setAudioCurrentTime = (seconds) => {
  getAudio().currentTime = seconds
}

const setStoreCurrentTime = (seconds) => {
  audioStore.currentTime = seconds
}

export {
  getAudio,
  setVolume,
  setAudioVolume,
  setStoreVolume,
  getVolumeLevel,
  setAudioSource,
  changeIsPlaying,
  setIsPlaying,
  setStoreIsPlaying,
  setAudioIsPlaying,
  getIsPlaying,
  setCurrentTime,
  setAudioCurrentTime,
  setStoreCurrentTime
}
