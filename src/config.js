import commonConfig from 'common/config'
export default {
  ...commonConfig,

  defaultVolumeLevel: 100, // 0..100
  defaultIsPlaying: !commonConfig.isDebugMode,
}
