import i18n from 'common/i18n'
import { useAppStore } from 'src/stores/app-store'
import { receiveElectronEvent } from 'src/helpers/electron-event-helper'
import { createPinia } from 'pinia'
import events from 'common/events'

export default ({ app }) => {
  const appStore = useAppStore()
  const { t, changeLanguage } = i18n

  app.config.globalProperties.$t = (key) => {
    appStore.localeId // Нужно, чтобы элемент перерисовывался в зависимости от установленной локали
    return t(key)
  }

  app.config.globalProperties.$changeLanguage = (localeId) => {
    appStore.localeId = localeId
    changeLanguage(localeId)
  }

  // Переключаем текущую локаль по событию из трея
  receiveElectronEvent(window, events.localeChanged, (data) => app.config.globalProperties.$changeLanguage(data.localeId))
}

// Из компонентов можно использовать функции $t и $changeLocale согласно инструкции
// https://quasar.dev/options/app-internationalization/#how-to-use
