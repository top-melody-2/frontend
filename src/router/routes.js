
const routes = [
  {
    path: '/',
    redirect: '/mini'
  },
  {
    path: '/mini',
    component: () => import('layouts/MiniPlayerLayout.vue'),
    children: [
      { path: '', component: () => import('pages/MiniPlayerPage.vue') }
    ]
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
