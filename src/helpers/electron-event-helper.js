const sendElectronEvent = (window, type, data = null) => {
  window.api.send("toMain", { type: type, data: data })
}

const receiveElectronEvent = (window, type, func) => {
  window.api.receive('fromMain', (data) => {
    if (data.type === type) {
      func(data.data)
    }
  })
}

export { sendElectronEvent, receiveElectronEvent }
