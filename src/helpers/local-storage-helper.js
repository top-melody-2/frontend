const getFromLocalStorage = (key, defaultValue) => {
  const value = window.localStorage.getItem(key)
  if (!value) {
    return defaultValue
  }

  return JSON.parse(window.localStorage.getItem(key))
}

const setToLocalStorage = (key, value) => {
  window.localStorage.setItem(key, JSON.stringify(value))
}

const removeFromLocalStorage = (key) => {
  window.localStorage.removeItem(key)
}

const clearLocalStorage = () => {
  window.localStorage.clear()
}

export { getFromLocalStorage, setToLocalStorage, removeFromLocalStorage, clearLocalStorage }
