import { defineStore } from 'pinia'

export const useTracksStore = defineStore('tracks', {
  state: () => ({
    current: {
      id: 0,
      authors: [],
      name: '',
      duration: 0,
      streamUrl: '',
      canLike: false,
      canDislike: false,
    },
    isNextTrackLoading: false,
    isPrevTrackLoading: false,
    isLikeTrackLoading: false,
    isDislikeTrackLoading: false,
  }),
});
