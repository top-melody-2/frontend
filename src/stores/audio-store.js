import { defineStore } from 'pinia'
import { getFromLocalStorage } from 'src/helpers/local-storage-helper'
import config from 'src/config'

export const useAudioStore = defineStore('audio', {
  state: () => ({
    volume: getFromLocalStorage('volume', config.defaultVolumeLevel),
    isPlaying: config.defaultIsPlaying,
    currentTime: 0,
    isTrackLoading: false,
  }),
});
