import { defineStore } from 'pinia'
import config from 'src/config'

export const useAppStore = defineStore('app', {
  state: () => ({
    localeId: config.defaultLocaleId,
    isDragAndDropMode: config.defaultIsDragAndDropMode,
  }),
});
