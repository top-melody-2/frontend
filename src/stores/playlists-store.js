import { defineStore } from 'pinia'

export const usePlaylistsStore = defineStore('playlists', {
  state: () => ({
    list: [],
    currentId: null,
  }),
});
