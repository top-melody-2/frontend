import { getAudio, setStoreCurrentTime, setStoreIsPlaying, getVolumeLevel, setStoreVolume, setAudioIsPlaying } from 'src/facades/audio'
import { requestNextTrack, requestPrevTrack } from 'src/facades/tracks'
import { useAudioStore } from 'src/stores/audio-store'

const listenAudioEvents = () => {
  listenOnTimeUpdate()
  listenOnPause()
  listenOnPlay()
  listenOnVolumeChange()
  listenOnPreviousTrack()
  listenOnNextTrack()
  listenOnEnded()
  listenOnProgress()
  listenOnCanPlay()
}

let audioStore = useAudioStore()

const listenOnTimeUpdate = () => {
  getAudio().ontimeupdate = () => setStoreCurrentTime(getAudio().currentTime)
}

const listenOnPause = () => {
  getAudio().onpause = () => setStoreIsPlaying(false)
}

const listenOnPlay = () => {
  getAudio().onplay = () => setStoreIsPlaying(true)
}

const listenOnVolumeChange = () => {
  getAudio().onvolumechange = () => setStoreVolume(getVolumeLevel())
}

const listenOnNextTrack = () => {
  navigator.mediaSession.setActionHandler('nexttrack', () => requestNextTrack())
}

const listenOnPreviousTrack = () => {
  navigator.mediaSession.setActionHandler('previoustrack', () => requestPrevTrack())
}

const listenOnEnded = () => {
  getAudio().onended = () => requestNextTrack().then(() => setAudioIsPlaying(true))
}

const listenOnProgress = () => {
  const isHaveFutureDataStateNumber = 3
  getAudio().onprogress = () => audioStore.isTrackLoading = getAudio().readyState < isHaveFutureDataStateNumber
}

const listenOnCanPlay = () => {
  getAudio().oncanplay = () => audioStore.isTrackLoading = false
}

export default listenAudioEvents
