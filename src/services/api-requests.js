import { getPlaylists } from 'common/services/api-requests'
import api from 'common/services/api'
import { sendElectronEvent } from 'src/helpers/electron-event-helper'
import events from 'common/events'

api.interceptors.response.use(
  // middleware для then()
  (response) => {
    if (response && response.data && !response.data.success && !!response.data.error) {
      sendElectronEvent(window, events.showNotify, response.error)
    }

    return response
  },
  // middleware для catch()
  (error) => {
    if (error.response && error.response.data && !error.response.data.success && !!error.response.data.error) {
      sendElectronEvent(window, events.showNotify, error.response.data.error)
    } else if (error && error.message) {
      sendElectronEvent(window, events.showNotify, error.message)
    }

    return Promise.reject(error)
  }
)

const getNextTrack = async (playlistId) => {
  let track = null

  await api.get(`/playlists/${playlistId}/tracks/next`)
    .then((response) => {
      track = response.data.response
    })
    .catch(() => {})

  return track
}

const getPrevTrack = async (playlistId) => {
  let track = null

  await api.get(`/playlists/${playlistId}/tracks/prev`)
    .then((response) => {
      track = response.data.response
    })
    .catch(() => {})

  return track
}

const likeTrack = async (trackId) => {
  let message = null

  await api.patch(`/tracks/${trackId}/like`)
    .then((response) => {
      message = response.data.response
    })
    .catch(() => {})

  return message
}

const dislikeTrack = async (trackId) => {
  let message = null

  await api.patch(`/tracks/${trackId}/dislike`)
    .then((response) => {
      message = response.data.response
    })
    .catch(() => {})

  return message
}

const getTrackStream = async (trackId) => {
  return api.get(`/tracks/${trackId}/stream`)
}

export {
  getPlaylists,
  getNextTrack,
  getPrevTrack,
  likeTrack,
  dislikeTrack
}
