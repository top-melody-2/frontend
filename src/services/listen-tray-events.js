import { receiveElectronEvent } from 'src/helpers/electron-event-helper'
import { setIsPlaying } from 'src/facades/audio'
import { setCurrentPlaylistId } from 'src/facades/playlists'
import { requestNextTrack, requestPrevTrack, likeCurrentTrack, dislikeCurrentTrack } from 'src/facades/tracks'
import { useAppStore } from 'src/stores/app-store'
import events from 'common/events'

const appStore = useAppStore()

const listenTrayEvents = () => {
  listenPlaylistChangedEvent()
  listenIsDragAndDropModeEvent()
}

const listenPlaylistChangedEvent = () => receiveElectronEvent(window, events.playlistChanged, (data) => setCurrentPlaylistId(data.playlistId))
const listenIsDragAndDropModeEvent = () => receiveElectronEvent(window, events.dragAndDropModeChanged, (data) => appStore.isDragAndDropMode = data.isDragAndDropMode)

export default listenTrayEvents
