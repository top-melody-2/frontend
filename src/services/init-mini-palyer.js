import { requestPlaylists } from 'src/facades/playlists'
import listerAudioEvents from 'src/services/listen-audio-events'
import listenTrayEvents from 'src/services/listen-tray-events'
import { requestNextTrack } from 'src/facades/tracks'
import { setIsPlaying, setAudioVolume } from 'src/facades/audio'
import config from 'src/config'
import { useAudioStore } from 'src/stores/audio-store'
import { sendElectronEvent } from 'src/helpers/electron-event-helper'
import events from 'common/events'

const initMiniPlayer = async () => {
  await requestPlaylists().then(() => {
    setIsPlaying(config.defaultIsPlaying)
    setAudioVolume(useAudioStore().volume)
    listerAudioEvents()
    listenTrayEvents()
  })

  sendElectronEvent(window, events.miniPlayerReady)
}

export default initMiniPlayer
