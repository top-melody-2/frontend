const { configure } = require('quasar/wrappers');
const path = require('node:path')

module.exports = configure(function (/* ctx */) {
  return {
    boot: [
      'i18n',
    ],
    css: [
      'app.scss',
    ],
    extras: [
      'roboto-font',
      'material-icons',
    ],
    build: {
      target: {
        browser: [ 'es2019', 'edge88', 'firefox78', 'chrome87', 'safari13.1' ],
        node: 'node20',
      },
      alias: {
        common: path.join(__dirname, './common'),
      },
      vueRouterMode: 'history',
    },
    devServer: {
      open: true,
    },
    framework: {
      config: {},
      plugins: [],
    },
    animations: [],
    ssr: {
      pwa: false,
      prodPort: 3000,
      middlewares: [
        'render',
      ],
    },
    pwa: {
      workboxMode: 'generateSW',
      injectPwaMetaTags: true,
      swFilename: 'sw.js',
      manifestFilename: 'manifest.json',
      useCredentialsForManifestTag: false,
    },
    cordova: {},
    capacitor: {
      hideSplashscreen: true,
    },
    electron: {
      inspectPort: 5858,
      bundler: 'builder',
      packager: {},
      builder: {
        appId: 'aa-chernyh.top-melody',
        icon: 'public/favicon.svg',
        linux: {
          target: [
            {
              target: 'AppImage',
              arch: ['x64', 'arm64'],
            },
            {
              target: 'deb',
              arch: ['x64', 'arm64'],
            },
            {
              target: 'rpm',
              arch: ['x64', 'arm64'],
            },
          ],
          category: 'AudioVideo',
        },
      },
    },
    bex: {
      contentScripts: [
        'my-content-script',
      ],
    },
  }
});
