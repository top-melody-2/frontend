import packageConfig from './../package.json'
const isDebugMode = false
const environment = 'dev'

export default {
  appName: packageConfig.productName,
  version: packageConfig.version,
  authors: [packageConfig.author],
  description: packageConfig.description,
  apiUrl: environment == 'prod' ? 'http://api.black-cloud.space' : 'http://api.top-melody.local',
  isDebugMode: isDebugMode,
  defaultLocaleId: 'en',
  defaultIsDragAndDropMode: false,
}
