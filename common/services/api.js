import axios from 'axios'
import config from 'common/config'

const api = axios.create({ baseURL: config.apiUrl })

export default api