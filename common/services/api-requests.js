import api from 'common/services/api'

const getPlaylists = async () => {
  let playlists = []

  await api.get('/playlists')
    .then((response) => {
      playlists = response.data.response
    })
    .catch((error) => { throw error })

  return playlists
}

export { getPlaylists }
