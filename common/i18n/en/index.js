export default {
  'Show app': 'Show app',
  'Minimize': 'Minimize',
  'Always on top': 'Always on top',
  'Drag and drop mode': 'Drag and drop mode',
  'Previous track': 'Previous track',
  'Play': 'Play',
  'Pause': 'Pause',
  'Next track': 'Next track',
  'Like': 'Like',
  'Dislike': 'Dislike',
  'Playlist': 'Playlist',
  'About': 'About',
  'Quit': 'Quit',
  'Toggle dev tools': 'Toggle dev tools',
  'Language': 'Language',
  'No connection to server': 'No connection to server',
  'Minimalist app for listening to music': 'Minimalist app for listening to music',
  'Alexandr Chernyh': 'Alexandr Chernyh',
  'Drag the window': 'Drag the window',
}
