import i18n from 'i18next'
import en from 'common/i18n/en'
import ru from 'common/i18n/ru'
import config from 'common/config'

let isInited = false

if (!isInited) {
  i18n.init({
    lng: config.defaultLocaleId,
    resources: {
      en: {translation: en},
      ru: {translation: ru}
    }
  })
  
  isInited = true
}

export default i18n
