export default {
  playlistChanged: 'playlist-changed',
  dragAndDropModeChanged: 'drag-and-drop-mode-changed',
  miniPlayerReady: 'mini-player-ready',
  localeChanged: 'locale-changed',
  showNotify: 'show-notify',
}