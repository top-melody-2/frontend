import { ipcMain } from 'electron'
import appStore from 'src-electron/stores/app-store'
import { sendElectronEvent, receiveElectronEvent } from 'src-electron/helpers/electron-event-helper'
import showNotify from 'src-electron/helpers/show-notify-helper'
import events from 'common/events'

const listenMiniPlayerEvents = (mainWindow) => {
  listenMiniPlayerReadyEvent(mainWindow)
  listenShowNotifyEvent(mainWindow)
}

const listenMiniPlayerReadyEvent = (mainWindow) => {
  receiveElectronEvent(ipcMain, events.miniPlayerReady, () => {
    sendElectronEvent(mainWindow, events.playlistChanged, { playlistId: appStore.get('playlistId') })
    sendElectronEvent(mainWindow, events.localeChanged, { localeId: appStore.get('localeId') })
    sendElectronEvent(mainWindow, events.dragAndDropModeChanged, { isDragAndDropMode: appStore.get('isDragAndDropMode') })
  })
}

const listenShowNotifyEvent = (mainWindow) => {
  receiveElectronEvent(ipcMain, events.showNotify, (message) => {
    showNotify(message)
  })
}

export default listenMiniPlayerEvents