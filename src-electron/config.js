import commonConfig from 'common/config'
export default {
  ...commonConfig,

  aboutIconPath: 'icons/favicon-96x96.png',
  appIconPath: 'icons/favicon-96x96.png',
  trayIconPath: 'icons/favicon-32x32.png',
  width: commonConfig.isDebugMode ? 1280 : 500,
  height: commonConfig.isDebugMode ? 720 : 62,
  defaultXRightMarginInPx: 50,
  defaultYRightMarginInPx: 50,
  defaultIsAlwaysOnTop: false,
}
