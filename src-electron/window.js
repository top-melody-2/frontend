import path from 'path'
import { BrowserWindow, screen } from 'electron'
import createTray from 'src-electron/tray.js'
import { getPathToStatic } from 'src-electron/helpers/static-helper'
import config from 'src-electron/config'
import appStore from 'src-electron/stores/app-store'
import listenMiniPlayerEvents from 'src-electron/services/listenMiniPlayerEvents'

const createWindow = async () => {
  const width = config.width
  const height = config.height
  let mainWindow = new BrowserWindow({
    index: process.env.APP_URL,
    title: config.appName,
    icon: getPathToStatic(config.appIconPath),
    maxWidth: width,
    minWidth: width,
    maxHeight: height,
    minHeight: height,
    x: screen.getPrimaryDisplay().bounds.width - width - (appStore.get('lastXRightMarginInPx') || config.defaultXRightMarginInPx),
    y: screen.getPrimaryDisplay().bounds.height - height - (appStore.get('lastYRightMarginInPx') || config.defaultYRightMarginInPx),
    frame: false,
    transparent: true,
    useContentSize: true,
    alwaysOnTop: appStore.get('isAlwaysOnTop'),
    webPreferences: {
      contextIsolation: true,
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD),
    }
  })

  mainWindow.setMenu(null)
  mainWindow.loadURL(process.env.APP_URL)
  mainWindow.focus()

  await createTray(mainWindow)

  if (config.isDebugMode) {
    mainWindow.webContents.openDevTools();
  }

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  await listenMiniPlayerEvents(mainWindow)

  return mainWindow
}

export default createWindow
