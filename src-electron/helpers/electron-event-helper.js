const sendElectronEvent = (mainWindow, type, data) => {
  mainWindow.webContents.send(
    "fromMain",
    { type: type, data: data }
  )
}

const receiveElectronEvent = (ipcMain, type, func) => {
  ipcMain.on(
    "toMain",
    (event, data) => {
      if (data.type === type) {
        func(data.data)
      }
    }
  )
}

export { sendElectronEvent, receiveElectronEvent }
