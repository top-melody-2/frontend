import EventEmitter from 'node:events'

const eventEmitter = new EventEmitter()
const eventPlaylistChanged = 'playlist-changed'
const eventAlwaysOnTopChanged = 'always-on-top-changed'
const eventLocaleChanged = 'language-changed'
const eventDragAndDropModeChanged = 'drag-and-drop-mode-changed'

export { eventEmitter, eventPlaylistChanged, eventAlwaysOnTopChanged, eventLocaleChanged, eventDragAndDropModeChanged }