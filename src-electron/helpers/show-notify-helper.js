import { Notification } from 'electron'
import config from 'src-electron/config'

const showNotify = (message) => {
  new Notification({
    title: config.appName,
    body: message
  }).show()
}

export default showNotify