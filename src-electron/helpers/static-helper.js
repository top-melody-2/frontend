import path from 'path'

const getPathToStatic = (subPath) => {
  const publicFolder = path.resolve(__dirname, process.env.QUASAR_PUBLIC_FOLDER)
  return path.join(publicFolder, '/', subPath)
}

export { getPathToStatic }
