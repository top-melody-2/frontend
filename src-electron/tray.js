import { sendElectronEvent, receiveElectronEvent } from 'src-electron/helpers/electron-event-helper'
import { eventEmitter, eventPlaylistChanged, eventAlwaysOnTopChanged, eventLocaleChanged, eventDragAndDropModeChanged } from 'src-electron/helpers/event-emitter-helper'
import { Menu, Tray, ipcMain, screen } from 'electron'
import { getPathToStatic } from 'src-electron/helpers/static-helper'
import { getPlaylists } from 'src-electron/api-requests'
import config from 'src-electron/config'
import i18n from 'common/i18n'
import locales from 'common/i18n/locales'
import appStore from 'src-electron/stores/app-store'
import events from 'common/events'

let playlists

const createTray = async (mainWindow) => {
  await init(mainWindow)

  let tray = new Tray(getPathToStatic(config.trayIconPath))
  tray.setToolTip(config.description)
  tray.setTitle(config.appName)
  tray.setContextMenu(getTrayContextMenu(mainWindow))
  eventEmitter.on(eventPlaylistChanged, () => {
    tray.setContextMenu(getTrayContextMenu(mainWindow))
  })
  eventEmitter.on(eventAlwaysOnTopChanged, () => {
    mainWindow.setAlwaysOnTop(appStore.get('isAlwaysOnTop'))
    tray.setContextMenu(getTrayContextMenu(mainWindow))
  })
  eventEmitter.on(eventLocaleChanged, () => {
    tray.setContextMenu(getTrayContextMenu(mainWindow))
  })
  eventEmitter.on(eventDragAndDropModeChanged, () => {
    tray.setContextMenu(getTrayContextMenu(mainWindow))
  })

  return tray
}

const init = async (mainWindow) => {
  i18n.changeLanguage(appStore.get('localeId'))
  eventEmitter.emit(eventLocaleChanged)
  await requestPlaylists(mainWindow)
}

const requestPlaylists = async (mainWindow) => {
  if (playlists !== undefined) {
    return
  }

  try {
    playlists = await getPlaylists()
  } catch (error) {
    playlists = []
  }

  const isPlaylistIdExistInPlaylists = playlists.map(playlistDTO => playlistDTO.id).includes(appStore.get('playlistId'))
  if (!isPlaylistIdExistInPlaylists && playlists.length > 0) {
    appStore.set('playlistId', playlists[0].id)

    // Бросим event, чтобы изменить плейлист в приложении
    sendElectronEvent(mainWindow, events.playlistChanged, { playlistId: playlists[0].id })
  }
}

const getTrayContextMenu = (mainWindow) => {
  let contextMenu = [
    { label: i18n.t('Show app'), type: 'normal', click: () => {
      mainWindow.isMinimized() ? mainWindow.restore() : mainWindow.focus()
    }},
    { label: i18n.t('Minimize'), type: 'normal', click: () => {
      if (appStore.get('isAlwaysOnTop') !== false) {
        appStore.set('isAlwaysOnTop', false)
        eventEmitter.emit(eventAlwaysOnTopChanged)
      }

      mainWindow.minimize()
    }},
    { label: i18n.t('Always on top'), type: 'checkbox', checked: appStore.get('isAlwaysOnTop'), click: () => {
      appStore.set('isAlwaysOnTop', !appStore.get('isAlwaysOnTop'))
      eventEmitter.emit(eventAlwaysOnTopChanged)

      if (mainWindow.isMinimized()) {
        mainWindow.restore()
      }
    }},
    { label: i18n.t('Drag and drop mode'), type: 'checkbox', checked: appStore.get('isDragAndDropMode'), click: () => {
        appStore.set('isDragAndDropMode', !appStore.get('isDragAndDropMode'))
        eventEmitter.emit(eventDragAndDropModeChanged)

        appStore.set('lastXRightMarginInPx', screen.getPrimaryDisplay().bounds.width - mainWindow.getPosition()[0] - config.width)
        appStore.set('lastYRightMarginInPx', screen.getPrimaryDisplay().bounds.height - mainWindow.getPosition()[1] - config.height)

        // Бросим event, чтобы изменить drag-and-drop mode в приложении
        sendElectronEvent(mainWindow, events.dragAndDropModeChanged, { isDragAndDropMode: appStore.get('isDragAndDropMode') })
    }},
    { type: 'separator' },
    { label: i18n.t('Playlist'), submenu: getPlaylistSubmenuOptions(mainWindow) },
    { type: 'separator' },
    { label: i18n.t('Language'), submenu: getLocalesSubmenuOptions(mainWindow) },
    { label: i18n.t('About'), type: 'normal', role: 'about' },
    { label: i18n.t('Quit'), type: 'normal', role: 'quit' },
  ]

  if (config.isDebugMode) {
    // Воткнёт Dev tools на 15-ю позицию
    contextMenu.splice(15, 0, { label: i18n.t('Toggle dev tools'), type: 'normal', role: 'toggleDevTools' })
  }

  return Menu.buildFromTemplate(contextMenu)
}

const getPlaylistSubmenuOptions = (mainWindow) => {
  let options = []

  playlists.forEach((playlistDTO) => {
    options.push({
      label: playlistDTO.name,
      type: 'radio',
      click: () => {
        appStore.set('playlistId', playlistDTO.id)

        // Бросим event о смене плейлиста в приложение
        sendElectronEvent(mainWindow, events.playlistChanged, { playlistId: playlistDTO.id })

        // Бросим event, чтобы перерисовать меню в tray
        eventEmitter.emit(eventPlaylistChanged)
      },
      checked: appStore.get('playlistId') === playlistDTO.id
    })
  })

  return options
}

const getLocalesSubmenuOptions = (mainWindow) => {
  let options = []

  locales.forEach((dictItemDTO) => {
    options.push({
      label: dictItemDTO.label,
      type: 'radio',
      click: () => {
        appStore.set('localeId', dictItemDTO.value)
        i18n.changeLanguage(dictItemDTO.value)

        // Бросим event о смене лолкали в приложение
        sendElectronEvent(mainWindow, events.localeChanged, { localeId: dictItemDTO.value })

        // Бросим event, чтобы перерисовать меню в tray
        eventEmitter.emit(eventLocaleChanged)
      },
      checked: appStore.get('localeId') === dictItemDTO.value
    })
  })

  return options
}

export default createTray
