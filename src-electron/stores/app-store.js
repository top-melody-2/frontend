import Store from 'electron-store'
import config from 'src-electron/config'

export default new Store({
  playlistId: null,
  localeId: config.defaultLocaleId,
  isAlwaysOnTop: config.defaultIsAlwaysOnTop,
  isDragAndDropMode: config.defaultIsDragAndDropMode,
  lastXRightMarginInPx: null,
  lastYRightMarginInPx: null,
})
