import { app } from 'electron'
import os from 'os'
import { getPathToStatic } from 'src-electron/helpers/static-helper'
import createWindow from 'src-electron/window'
import config from 'src-electron/config'
import i18n from 'common/i18n'
import { eventEmitter, eventLocaleChanged } from 'src-electron/helpers/event-emitter-helper'

const getAboutPanelOptions = () => {
  return {
    applicationName: config.appName,
    applicationVersion: config.version,
    version: config.version,
    authors: config.authors.map(author => i18n.t(author.name)),
    copyright: i18n.t(config.description),
    iconPath: getPathToStatic(config.aboutIconPath),
  }
}
app.setAboutPanelOptions(getAboutPanelOptions())

eventEmitter.on(eventLocaleChanged, () => {
  app.setAboutPanelOptions(getAboutPanelOptions())
})

let mainWindow
app.whenReady().then(() => {
  mainWindow = createWindow()
})

const platform = process.platform || os.platform()
app.on('window-all-closed', () => {
  if (platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
