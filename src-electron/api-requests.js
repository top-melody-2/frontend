import { getPlaylists } from '../common/services/api-requests'

export {
  getPlaylists
}