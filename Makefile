default: dev-start

build-container:
	docker build -t tm2-front:latest ./docker
dev-start:
	docker run -v `pwd`:/app -d -it -p 9000:9000 --name tm2-front --rm tm2-front quasar dev
dev-stop:
	docker stop tm2-front
dev-quasar-local:
	docker run -v `pwd`:/app -it tm2-front rm -rf .quasar
	quasar dev -m electron
command:
	docker run --rm -it -v `pwd`:/app tm2-front $(c)
install:
	docker run -v `pwd`:/app -it --name tm2-front --rm tm2-front yarn install
build:
	docker run -v `pwd`:/app -it --name tm2-front --rm tm2-front quasar build -m electron
